import nodemailer from "nodemailer";

const emailService = {
  setupTransport: (config) => {
    let transport;
    if (config.type == "GMAIL") {
      transport = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: config.user,
          pass: config.password,
        },
      });
    } else if (config.type == "SMTP") {
      transport = nodemailer.createTransport({
        host: config.host,
        port: config.port,
        auth: {
          user: config.user,
          pass: config.password,
        },
      });
    }
    return transport;
  },
  sendEmail: async (config, emailTo, emailFrom, name, username, password) => {
    let transport = emailService.setupTransport(config);
    let mailConfig = {
      to: emailTo,
      from: emailFrom,
      subject: "Reminder to complete Privacy Questionnaire",
      text: `Hi ${name}! Your login details are as follows: \nUsername: ${username}\nPassword: ${password}`,
    };
    // Add URL link (fetch domain)
    if (!transport) return false;
    const resp = await transport.sendMail(mailConfig);
  },
};

export default emailService;
