import { withIronSessionApiRoute } from "iron-session/next";

export default function withSession(handler) {
  return withIronSessionApiRoute(handler, {
    password: process.env.SECRET_COOKIE_PASSWORD,
    cookieName: "109_user_session",
    cookieOptions: {
      secure: false
    },
  });
}