import { useEffect } from "react";
import Router from "next/router";
import useSWR from "swr";

export default function useUser({
  adminOnly = false,
  redirectTo = "",
  redirectIfNoAuth = true
} = {}) {
  const { data: user, mutate: mutateUser, error } = useSWR(`/api/login/${adminOnly ? "admin" : "user"}`);

  useEffect(() => {
    if (!redirectTo || (!user && !error)) return;

    if (
      (!redirectIfNoAuth && user?.isLoggedIn && (adminOnly == user?.isAdmin)) || // logged in
      (adminOnly && !user?.isAdmin && redirectIfNoAuth) || // Admin page and non-admin user
      (error && redirectIfNoAuth) // error i.e not logged in and a redirect
    ) {
      Router.push(redirectTo);
    }
  }, [user, error]);

  return { user, mutateUser };
}
