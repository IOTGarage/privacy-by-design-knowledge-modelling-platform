/** @type {import('next').NextConfig} */
const nextConfig = {
  async redirects() {
    return [
      {
        source: "/admin",
        destination: "/admin/users",
        permanent: true
      },
      {
        source: "/",
        destination: "/questions",
        permanent: true
      },
    ];
  },
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  env: {
    SECRET_COOKIE_PASSWORD: 'HK5CVr60zxvqbpuicBWBrGpiynwJWpzR',
  },
};

module.exports = nextConfig;
