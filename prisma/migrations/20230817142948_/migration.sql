-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Master" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "profession" TEXT NOT NULL,
    "dev_experience" INTEGER NOT NULL,
    "privacy_experience" INTEGER NOT NULL,
    "notes" TEXT NOT NULL,
    "pp1" TEXT NOT NULL,
    "pp1_description" TEXT NOT NULL,
    "pp2" TEXT NOT NULL,
    "pp2_description" TEXT NOT NULL,
    "relationship" TEXT NOT NULL,
    "explanation" TEXT NOT NULL DEFAULT ''
);
INSERT INTO "new_Master" ("date", "dev_experience", "email", "id", "name", "notes", "pp1", "pp1_description", "pp2", "pp2_description", "privacy_experience", "profession", "relationship") SELECT "date", "dev_experience", "email", "id", "name", "notes", "pp1", "pp1_description", "pp2", "pp2_description", "privacy_experience", "profession", "relationship" FROM "Master";
DROP TABLE "Master";
ALTER TABLE "new_Master" RENAME TO "Master";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
