/*
  Warnings:

  - You are about to drop the column `experience` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `experience` on the `Master` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "admin" BOOLEAN NOT NULL DEFAULT false,
    "profession" TEXT NOT NULL,
    "dev_experience" INTEGER NOT NULL DEFAULT 1,
    "privacy_experience" INTEGER NOT NULL DEFAULT 1,
    "Notes" TEXT NOT NULL DEFAULT 'Notes',
    "submitted" BOOLEAN NOT NULL DEFAULT false,
    "lastreminded" DATETIME NOT NULL
);
INSERT INTO "new_User" ("admin", "email", "id", "lastreminded", "name", "password", "profession", "submitted", "username") SELECT "admin", "email", "id", "lastreminded", "name", "password", "profession", "submitted", "username" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");
CREATE TABLE "new_Master" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "profession" TEXT NOT NULL,
    "dev_experience" INTEGER NOT NULL DEFAULT 1,
    "privacy_experience" INTEGER NOT NULL DEFAULT 1,
    "Notes" TEXT NOT NULL DEFAULT 'Notes',
    "pp1" TEXT NOT NULL,
    "pp1_description" TEXT NOT NULL DEFAULT 'Something',
    "pp2" TEXT NOT NULL,
    "pp2_description" TEXT NOT NULL DEFAULT 'Something',
    "relationship" TEXT NOT NULL
);
INSERT INTO "new_Master" ("date", "email", "id", "name", "pp1", "pp2", "profession", "relationship") SELECT "date", "email", "id", "name", "pp1", "pp2", "profession", "relationship" FROM "Master";
DROP TABLE "Master";
ALTER TABLE "new_Master" RENAME TO "Master";
CREATE TABLE "new_PP" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "principle" TEXT NOT NULL,
    "description" TEXT NOT NULL DEFAULT 'Something!'
);
INSERT INTO "new_PP" ("id", "principle") SELECT "id", "principle" FROM "PP";
DROP TABLE "PP";
ALTER TABLE "new_PP" RENAME TO "PP";
CREATE UNIQUE INDEX "PP_principle_key" ON "PP"("principle");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
