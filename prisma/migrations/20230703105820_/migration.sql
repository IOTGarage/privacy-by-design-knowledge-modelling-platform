/*
  Warnings:

  - You are about to drop the column `question` on the `PP` table. All the data in the column will be lost.
  - Added the required column `text` to the `Relationship` table without a default value. This is not possible if the table is not empty.
  - Added the required column `principle` to the `PP` table without a default value. This is not possible if the table is not empty.
  - Made the column `experience` on table `User` required. This step will fail if there are existing NULL values in that column.
  - Made the column `profession` on table `User` required. This step will fail if there are existing NULL values in that column.

*/
-- CreateTable
CREATE TABLE "Master" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "date" DATETIME NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "profession" TEXT NOT NULL,
    "experience" INTEGER NOT NULL,
    "pp1" TEXT NOT NULL,
    "pp2" TEXT NOT NULL,
    "relationship" TEXT NOT NULL
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Relationship" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "text" TEXT NOT NULL
);
INSERT INTO "new_Relationship" ("id") SELECT "id" FROM "Relationship";
DROP TABLE "Relationship";
ALTER TABLE "new_Relationship" RENAME TO "Relationship";
CREATE TABLE "new_PP" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "principle" TEXT NOT NULL
);
INSERT INTO "new_PP" ("id") SELECT "id" FROM "PP";
DROP TABLE "PP";
ALTER TABLE "new_PP" RENAME TO "PP";
CREATE UNIQUE INDEX "PP_principle_key" ON "PP"("principle");
CREATE TABLE "new_User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "profession" TEXT NOT NULL,
    "experience" INTEGER NOT NULL
);
INSERT INTO "new_User" ("email", "experience", "id", "name", "password", "profession", "username") SELECT "email", "experience", "id", "name", "password", "profession", "username" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
