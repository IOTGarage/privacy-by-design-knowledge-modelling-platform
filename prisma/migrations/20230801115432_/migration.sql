-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "admin" BOOLEAN NOT NULL DEFAULT false,
    "profession" TEXT NOT NULL,
    "dev_experience" INTEGER NOT NULL,
    "privacy_experience" INTEGER NOT NULL,
    "Notes" TEXT NOT NULL,
    "submitted" BOOLEAN NOT NULL DEFAULT false,
    "lastreminded" DATETIME NOT NULL
);
INSERT INTO "new_User" ("Notes", "admin", "dev_experience", "email", "id", "lastreminded", "name", "password", "privacy_experience", "profession", "submitted", "username") SELECT "Notes", "admin", "dev_experience", "email", "id", "lastreminded", "name", "password", "privacy_experience", "profession", "submitted", "username" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");
CREATE TABLE "new_Master" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "profession" TEXT NOT NULL,
    "dev_experience" INTEGER NOT NULL,
    "privacy_experience" INTEGER NOT NULL,
    "Notes" TEXT NOT NULL,
    "pp1" TEXT NOT NULL,
    "pp1_description" TEXT NOT NULL,
    "pp2" TEXT NOT NULL,
    "pp2_description" TEXT NOT NULL,
    "relationship" TEXT NOT NULL
);
INSERT INTO "new_Master" ("Notes", "date", "dev_experience", "email", "id", "name", "pp1", "pp1_description", "pp2", "pp2_description", "privacy_experience", "profession", "relationship") SELECT "Notes", "date", "dev_experience", "email", "id", "name", "pp1", "pp1_description", "pp2", "pp2_description", "privacy_experience", "profession", "relationship" FROM "Master";
DROP TABLE "Master";
ALTER TABLE "new_Master" RENAME TO "Master";
CREATE TABLE "new_PP" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "principle" TEXT NOT NULL,
    "description" TEXT NOT NULL
);
INSERT INTO "new_PP" ("description", "id", "principle") SELECT "description", "id", "principle" FROM "PP";
DROP TABLE "PP";
ALTER TABLE "new_PP" RENAME TO "PP";
CREATE UNIQUE INDEX "PP_principle_key" ON "PP"("principle");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
