-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "admin" BOOLEAN NOT NULL DEFAULT false,
    "profession" TEXT NOT NULL,
    "experience" INTEGER NOT NULL,
    "submitted" BOOLEAN NOT NULL DEFAULT false
);
INSERT INTO "new_User" ("email", "experience", "id", "name", "password", "profession", "submitted", "username") SELECT "email", "experience", "id", "name", "password", "profession", "submitted", "username" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
