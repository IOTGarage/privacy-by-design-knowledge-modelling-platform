/*
  Warnings:

  - You are about to drop the column `Notes` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `Notes` on the `Master` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "admin" BOOLEAN NOT NULL DEFAULT false,
    "profession" TEXT NOT NULL,
    "dev_experience" INTEGER NOT NULL,
    "privacy_experience" INTEGER NOT NULL,
    "notes" TEXT NOT NULL DEFAULT 'Notes',
    "submitted" BOOLEAN NOT NULL DEFAULT false,
    "lastreminded" DATETIME NOT NULL
);
INSERT INTO "new_User" ("admin", "dev_experience", "email", "id", "lastreminded", "name", "password", "privacy_experience", "profession", "submitted", "username") SELECT "admin", "dev_experience", "email", "id", "lastreminded", "name", "password", "privacy_experience", "profession", "submitted", "username" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");
CREATE TABLE "new_Master" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "profession" TEXT NOT NULL,
    "dev_experience" INTEGER NOT NULL,
    "privacy_experience" INTEGER NOT NULL,
    "notes" TEXT NOT NULL DEFAULT 'Notes',
    "pp1" TEXT NOT NULL,
    "pp1_description" TEXT NOT NULL,
    "pp2" TEXT NOT NULL,
    "pp2_description" TEXT NOT NULL,
    "relationship" TEXT NOT NULL
);
INSERT INTO "new_Master" ("date", "dev_experience", "email", "id", "name", "pp1", "pp1_description", "pp2", "pp2_description", "privacy_experience", "profession", "relationship") SELECT "date", "dev_experience", "email", "id", "name", "pp1", "pp1_description", "pp2", "pp2_description", "privacy_experience", "profession", "relationship" FROM "Master";
DROP TABLE "Master";
ALTER TABLE "new_Master" RENAME TO "Master";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
