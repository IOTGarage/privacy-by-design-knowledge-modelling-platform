-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_User_Question" (
    "userid" INTEGER NOT NULL,
    "pqid" INTEGER NOT NULL,
    "answered" BOOLEAN NOT NULL DEFAULT false,

    PRIMARY KEY ("userid", "pqid"),
    CONSTRAINT "User_Question_userid_fkey" FOREIGN KEY ("userid") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "User_Question_pqid_fkey" FOREIGN KEY ("pqid") REFERENCES "PQ" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_User_Question" ("pqid", "userid") SELECT "pqid", "userid" FROM "User_Question";
DROP TABLE "User_Question";
ALTER TABLE "new_User_Question" RENAME TO "User_Question";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
