-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Master" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "profession" TEXT NOT NULL,
    "experience" INTEGER NOT NULL,
    "pp1" TEXT NOT NULL,
    "pp2" TEXT NOT NULL,
    "relationship" TEXT NOT NULL
);
INSERT INTO "new_Master" ("date", "email", "experience", "id", "name", "pp1", "pp2", "profession", "relationship") SELECT "date", "email", "experience", "id", "name", "pp1", "pp2", "profession", "relationship" FROM "Master";
DROP TABLE "Master";
ALTER TABLE "new_Master" RENAME TO "Master";
CREATE TABLE "new_User" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "username" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "admin" BOOLEAN NOT NULL DEFAULT false,
    "profession" TEXT NOT NULL,
    "experience" INTEGER NOT NULL,
    "submitted" BOOLEAN NOT NULL DEFAULT false,
    "lastreminded" DATETIME NOT NULL
);
INSERT INTO "new_User" ("admin", "email", "experience", "id", "lastreminded", "name", "password", "profession", "submitted", "username") SELECT "admin", "email", "experience", "id", "lastreminded", "name", "password", "profession", "submitted", "username" FROM "User";
DROP TABLE "User";
ALTER TABLE "new_User" RENAME TO "User";
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
