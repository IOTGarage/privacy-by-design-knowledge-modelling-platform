/*
  Warnings:

  - The primary key for the `PQ` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - Added the required column `id` to the `PQ` table without a default value. This is not possible if the table is not empty.

*/
-- CreateTable
CREATE TABLE "User_Question" (
    "userid" INTEGER NOT NULL,
    "pqid" INTEGER NOT NULL,

    PRIMARY KEY ("userid", "pqid"),
    CONSTRAINT "User_Question_userid_fkey" FOREIGN KEY ("userid") REFERENCES "User" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "User_Question_pqid_fkey" FOREIGN KEY ("pqid") REFERENCES "PQ" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_PQ" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "pp1id" INTEGER NOT NULL,
    "pp2id" INTEGER NOT NULL,
    CONSTRAINT "PQ_pp1id_fkey" FOREIGN KEY ("pp1id") REFERENCES "PP" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "PQ_pp2id_fkey" FOREIGN KEY ("pp2id") REFERENCES "PP" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_PQ" ("pp1id", "pp2id") SELECT "pp1id", "pp2id" FROM "PQ";
DROP TABLE "PQ";
ALTER TABLE "new_PQ" RENAME TO "PQ";
CREATE UNIQUE INDEX "PQ_id_pp1id_pp2id_key" ON "PQ"("id", "pp1id", "pp2id");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
