-- CreateTable
CREATE TABLE "Email_Setting" (
    "type" TEXT NOT NULL DEFAULT 'GMAIL',
    "host" TEXT NOT NULL,
    "port" TEXT NOT NULL,
    "secure" BOOLEAN NOT NULL,
    "user" TEXT NOT NULL,
    "password" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Email_Setting_type_key" ON "Email_Setting"("type");
