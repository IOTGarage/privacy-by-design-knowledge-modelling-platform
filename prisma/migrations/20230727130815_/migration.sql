/*
  Warnings:

  - Added the required column `id` to the `Email_Setting` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Email_Setting" (
    "id" INTEGER NOT NULL,
    "type" TEXT NOT NULL DEFAULT 'GMAIL',
    "host" TEXT NOT NULL,
    "port" TEXT NOT NULL,
    "secure" BOOLEAN NOT NULL,
    "user" TEXT NOT NULL,
    "password" TEXT NOT NULL
);
INSERT INTO "new_Email_Setting" ("host", "password", "port", "secure", "type", "user") SELECT "host", "password", "port", "secure", "type", "user" FROM "Email_Setting";
DROP TABLE "Email_Setting";
ALTER TABLE "new_Email_Setting" RENAME TO "Email_Setting";
CREATE UNIQUE INDEX "Email_Setting_id_key" ON "Email_Setting"("id");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
