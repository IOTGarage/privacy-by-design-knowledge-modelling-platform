import { useEffect, useState } from "react";
import Relationships from "./Relationships";

// {pp1: {principle: ""}, pp2: {principle: ""}}

export default function CurrentQuestion({
  pq,
  setRelationship,
  setExplanation,
  pqid,
  answered,
  explanation,
  rid,
}) {
  const [explanationText, setExplanationText] = useState(explanation);

  useEffect(() => {
    setExplanationText(explanation);
  }, [pqid])

  const editExplanation = (e) => {
    setExplanationText(e.target.value)
    setExplanation(e.target.value);
  }

  return (
    <div className="grid items-center md:grid-cols-6 max-md:grid-rows-3">
      <div className="md:col-span-2 row-span-1 space-y-2">
        <div>
          <span>{pq.pp1.principle}</span>
        </div>
        <div>
          <span className="opacity-70">{pq.pp1.description}</span>
        </div>
      </div>
      <div className="md:col-span-2 row-span-1">
        <span className="text-sm text-gray-300">If you think that none of the options are appropriate, please select the most relevant one.</span>
        <Relationships
          setRelationship={setRelationship}
          pqid={pqid}
          rid={rid}
          answered={answered}
        />
        <textarea
          type="text"
          name="text"
          id="text"
          value={explanationText}
          disabled={answered}
          className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white overflow-auto h-fit"
          placeholder="If you think that none of the options are appropriate, please define the ideal relationship between the given principles and explain why"
          required={true}
          onChange={editExplanation}
        />
      </div>
      <div className="md:col-span-2 row-span-1 space-y-2">
        <div>
          <span>{pq.pp2.principle}</span>
        </div>
        <div>
          <span className="opacity-70">{pq.pp2.description}</span>
        </div>
      </div>
    </div>
  );
}
