import { useEffect, useState } from "react";
import AsyncSelect from "react-select/async";
import { components } from "react-select";
import {
  ArrowLongLeftIcon,
  ArrowLongRightIcon,
  MagnifyingGlassIcon,
} from "@heroicons/react/24/outline";

export default function AssignPQ() {
  const [user, setUser] = useState(0);
  const [search, setSearch] = useState("");
  const [pqs, setPQs] = useState([]);
  const [filterAssign, setFilterAssign] = useState(false);
  const [filterAns, setFilterAns] = useState(false);
  const [page, setPage] = useState(1);
  const [lastpage, setLastPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [update, setUpdate] = useState(false);
  let usertimeout;
  let pqtimeout;

  const fetchUsers = (filter, cb) => {
    fetch(
      "/api/users?" +
      new URLSearchParams({
        count: 5,
        page: 1,
        search: filter,
      }),
      {
        method: "get",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((res) => res.json())
      .then(({ data }) => cb(data));
  };

  const UserOption = (props) => {
    const { username, email } = props.data;
    return (
      <components.Option {...props}>
        <span>{username}</span>
        {" - "}
        <span className="text-sm opacity-70">{email}</span>
      </components.Option>
    );
  };

  const UserSingleValue = (props) => {
    const { username, email } = props.getValue()[0];
    return (
      <components.SingleValue {...props}>
        <span>{username}</span>
        {" - "}
        <span className="text-sm opacity-70">{email}</span>
      </components.SingleValue>
    );
  };

  useEffect(() => {
    if (user != 0) {
      fetch(
        "/api/questions/allquestions?" +
        new URLSearchParams({
          count: 5,
          page,
          search,
          userid: user,
          filterAssign,
          filterAns,
        }),
        {
          method: "get",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((res) => res.json())
        .then(({ data, lastpage, total }) => {
          console.log(data);
          setPQs(data);
          setTotal(total);
          setLastPage(lastpage);
        });
    }
  }, [search, page, user, filterAssign, filterAns, update]);

  const nextButton = () => {
    if (page == lastpage) return;
    setPage(page + 1);
  };

  const prevButton = () => {
    if (page == 1) return;
    setPage(page - 1);
  };

  const doSearch = (e) => {
    clearTimeout(pqtimeout);
    pqtimeout = setTimeout(() => {
      setSearch(e.target.value);
    }, 700);
  };

  const toggleAssign = (pqid, toggle) => {
    fetch("/api/users/assign", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userid: user,
        pqid,
        type: toggle ? "remove" : "add", // Reverse
      }),
    })
      .then((res) => res.json())
      .then(({ error, data }) => {
        if (!error) setUpdate(!update);
        console.log(data);
      });
  };

  return (
    <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
      <div className="p-6 space-y-6 sm:p-8">
        <h1 className="text-xl">Assign Privacy Questions</h1>
        <form className="space-y-6" action="#" onSubmit={null}>
          <div className="w-full md:w-1/3">
            <label
              htmlFor="name"
              className="block mb-2 text-sm font-medium text-white"
            >
              Select User
            </label>
            <AsyncSelect
              instanceId={1}
              cacheOptions
              defaultOptions
              loadOptions={(e, cb) => {
                clearTimeout(usertimeout);
                usertimeout = setTimeout(() => {
                  fetchUsers(e, cb);
                }, 1000);
              }}
              getOptionLabel={(e) => `${e.username} ${e.email}`}
              getOptionValue={(e) => e.id}
              classNames={{
                control: () => "!border !border-gray-500",
                valueContainer: () => "bg-gray-800",
                indicatorsContainer: () => "bg-gray-800",
                input: () => "!text-white",
                menu: () => "z-1",
                menuList: () => "border border-gray-700 bg-gray-800 !p-0",
                option: () => "!bg-gray-800 hover:!bg-gray-700 truncate",
                singleValue: () => "!text-white",
                noOptionsMessage: () => "!text-white",
              }}
              onChange={(v) => setUser(v.id)}
              noOptionsMessage={() => "No User Found"}
              components={{
                SingleValue: UserSingleValue,
                Option: UserOption,
              }}
            />
          </div>
          <div className="">
            <label
              htmlFor="name"
              className="block mb-2 text-sm font-medium text-white"
            >
              Privacy Questions
            </label>
            <div className="overflow-x-auto">
              <div className="flex flex-wrap pb-4 space-x-2">
                <div className="relative mt-1">
                  <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <MagnifyingGlassIcon className="h-6 w-6 text-gray-200" />
                  </div>
                  <input
                    type="text"
                    id="table-search"
                    className="m-1 p-2 pl-10 text-sm border rounded-lg w-80 bg-gray-700 border-gray-600 placeholder-gray-400"
                    placeholder={"Search for questions"}
                    onChange={doSearch}
                  />
                </div>
                <div className="flex items-center mb-4 mt-4">
                  <input
                    id="default-checkbox"
                    type="checkbox"
                    checked={filterAssign}
                    onChange={() => {
                      setPage(1);
                      setFilterAssign(!filterAssign);
                    }}
                    className="w-4 h-4 text-blue-600 focus:ring-blue-600 ring-offset-gray-800 focus:ring-2 bg-gray-700 border-gray-600"
                  />
                  <label
                    htmlFor="default-checkbox"
                    className="ml-2 text-sm font-medium text-gray-300"
                  >
                    Filter by assigned
                  </label>
                </div>
                <div className="flex items-center mb-4 mt-4">
                  <input
                    id="default-checkbox"
                    type="checkbox"
                    checked={filterAns}
                    onChange={() => {
                      setPage(1);
                      setFilterAns(!filterAns);
                    }}
                    className="w-4 h-4 text-blue-600focus:ring-blue-600 ring-offset-gray-800 focus:ring-2 bg-gray-700 border-gray-600"
                  />
                  <label
                    htmlFor="default-checkbox"
                    className="ml-2 text-sm font-medium text-gray-300"
                  >
                    Filter by answered
                  </label>
                </div>
              </div>
              <table className="w-full text-sm text-left text-gray-400">
                <thead className="text-xs uppercase bg-gray-700 text-gray-400">
                  <tr>
                    <th scope="col" className="p-4">
                      Assign
                    </th>
                    <th scope="col" className="px-6 py-3">
                      PQ ID
                    </th>
                    <th scope="col" className="px-6 py-3">
                      PP 1
                    </th>
                    <th scope="col" className="px-6 py-3">
                      PP 2
                    </th>
                    <th scope="col" className="px-6 py-3">
                      Answered
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {pqs.length > 0 &&
                    pqs.map((p) => {
                      return (
                        <tr
                          className="border-b bg-gray-800 border-gray-700"
                          key={p.id}
                        >
                          <td
                            scope="row"
                            className="px-6 py-4 font-medium text-white"
                          >
                            <input
                              id="default-checkbox"
                              type="checkbox"
                              checked={p.assigned}
                              disabled={p.answered}
                              onChange={() => toggleAssign(p.id, p.assigned)}
                              className="w-4 h-4 text-blue-600focus:ring-blue-600 ring-offset-gray-800 focus:ring-2 bg-gray-700 border-gray-600"
                            />
                          </td>
                          <td className="px-6 py-4">{p.id}</td>
                          <td className="px-6 py-4">{p.pp1.principle}</td>
                          <td className="px-6 py-4">{p.pp2.principle}</td>
                          <td className="px-6 py-4">
                            {p.answered ? "Yes" : "No"}
                          </td>
                        </tr>
                      );
                    })}
                  {pqs.length < 1 && (
                    <tr className="border-b bg-gray-800 border-gray-700">
                      <td
                        scope="row"
                        className="px-6 py-4 font-medium text-white"
                      >
                        No Privacy Questions match your filters
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
              <nav
                className="flex items-center justify-between pt-4"
                aria-label="Table navigation"
              >
                <span className="text-sm font-normal text-gray-400">
                  Showing{" "}
                  <span className="font-semibold text-white">
                    {pqs.length > 0
                      ? pqs[0].id + " - " + pqs[pqs.length - 1].id
                      : 0 - 0}
                  </span>{" "}
                  of{" "}
                  <span className="font-semibold text-white">
                    {total}
                  </span>
                </span>
                <div className="flex">
                  <a
                    onClick={prevButton}
                    className="flex items-center justify-center px-3 h-8 mr-3 text-sm font-medium border rounded-lg bg-gray-800 border-gray-700 text-gray-400 hover:bg-gray-700 hover:text-white cursor-pointer space-x-1 select-none"
                  >
                    <ArrowLongLeftIcon className="h-6 w-5 text-gray-200" />
                    <span>Previous</span>
                  </a>
                  <a
                    onClick={nextButton}
                    className="flex items-center justify-center px-3 h-8 mr-3 text-sm font-medium border rounded-lg bg-gray-800 border-gray-700 text-gray-400 hover:bg-gray-700 hover:text-white cursor-pointer space-x-1 select-none"
                  >
                    <span>Next</span>
                    <ArrowLongRightIcon className="h-6 w-5 text-gray-200" />
                  </a>
                </div>
              </nav>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
