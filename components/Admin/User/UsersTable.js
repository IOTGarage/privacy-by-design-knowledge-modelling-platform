import {
  ArrowLongLeftIcon,
  ArrowLongRightIcon,
  MagnifyingGlassIcon,
} from "@heroicons/react/24/outline";
import { useEffect, useState } from "react";

export default function UsersTable({ update, setUpdate }) {
  const [users, setUsers] = useState([]);
  const [page, setPage] = useState(1);
  const [lastpage, setLastPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState("");
  let searchTimeout;

  useEffect(() => {
    async function getUsers() {
      await fetch(
        "/api/users?" +
          new URLSearchParams({
            count: 10,
            page,
            search,
          }),
        {
          method: "get",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((res) => res.json())
        .then(({ data, lastpage, total }) => {
          console.log(data);
          data && setUsers(data);
          setLastPage(lastpage);
          setTotal(total);
        });
    }
    getUsers();
  }, [page, search, update]);

  const sendReminder = (userid) => {
    fetch("/api/users/sendreminder", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        userid,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        !data?.error && setUpdate(!update);
        console.log(data);
      });
  };

  const doSearch = (e) => {
    clearTimeout(searchTimeout);
    searchTimeout = setTimeout(() => {
      setSearch(e.target.value);
    }, 700);
  };

  const timeDifference = (previous) => {
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = new Date() - new Date(previous);

    if (Math.round(elapsed / msPerYear) > 50) return "never";
    if (elapsed < msPerMinute) {
      return Math.round(elapsed / 1000) + " seconds ago";
    } else if (elapsed < msPerHour) {
      return Math.round(elapsed / msPerMinute) + " minutes ago";
    } else if (elapsed < msPerDay) {
      return Math.round(elapsed / msPerHour) + " hours ago";
    } else if (elapsed < msPerMonth) {
      return Math.round(elapsed / msPerDay) + " days ago";
    } else if (elapsed < msPerYear) {
      return Math.round(elapsed / msPerMonth) + " months ago";
    } else {
      return Math.round(elapsed / msPerYear) + " years ago";
    }
  };

  const nextButton = () => {
    if (page == lastpage) return;
    setPage(page + 1);
  };

  const prevButton = () => {
    if (page == 1) return;
    setPage(page - 1);
  };

  return (
    <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
      <div className="p-6 space-y-6 sm:p-8">
        <h1 className="text-xl">Users</h1>
        <div className="pb-4">
          <label htmlFor="table-search" className="sr-only">
            Search
          </label>
          <div className="relative mt-1">
            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              <MagnifyingGlassIcon className="h-6 w-6 text-gray-200" />
            </div>
            <input
              type="text"
              id="table-search"
              className="m-1 p-2 pl-10 text-sm border rounded-lg w-80 bg-gray-700 border-gray-600 placeholder-gray-400"
              placeholder={"Search for users"}
              onChange={doSearch}
            />
          </div>
        </div>
        <div className="w-full overflow-x-auto">
          <table className="w-full text-sm text-left text-gray-400">
            <thead className="text-xs uppercase bg-gray-700 text-gray-400">
              <tr>
                <th scope="col" className="px-6 py-3">
                  User ID
                </th>
                <th scope="col" className="px-6 py-3">
                  Name
                </th>
                <th scope="col" className="px-6 py-3">
                  Username
                </th>
                <th scope="col" className="px-6 py-3">
                  Email
                </th>
                <th scope="col" className="px-6 py-3">
                  Password
                </th>
                <th scope="col" className="px-6 py-3">
                  Submitted
                </th>
                <th scope="col" className="px-6 py-3">
                  Actions
                </th>
                <th scope="col" className="px-6 py-3 whitespace-nowrap">
                  Profession
                </th>
                <th scope="col" className="px-6 py-3 whitespace-nowrap">
                  Dev Experience
                </th>
                <th scope="col" className="px-6 py-3 whitespace-nowrap">
                  Privacy Experience
                </th>
                <th scope="col" className="px-6 py-3 whitespace-nowrap">
                  Notes
                </th>
              </tr>
            </thead>
            <tbody>
              {users.length > 0 &&
                users.map((u) => {
                  return (
                    <tr
                      className="border-b bg-gray-800 border-gray-700"
                      key={u.id}
                    >
                      <td
                        scope="row"
                        className="px-6 py-4 font-medium text-white"
                      >
                        {u.id}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">{u.name}</td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        {u.username}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">{u.email}</td>
                      <td className="px-6 py-4 whitespace-nowrap">{u.password}</td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        {u.submitted ? "Yes" : "No"}
                        <span className="opacity-75 italic whitespace-nowrap">
                          {" - reminded " + timeDifference(u.lastreminded)}
                        </span>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <a
                          className="underline cursor-pointer select-none"
                          onClick={() => sendReminder(u.id)}
                        >
                          Send Reminder
                        </a>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        {u.profession}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        {u.dev_experience}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        {u.privacy_experience}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">{u.notes}</td>
                    </tr>
                  );
                })}
              {users.length < 1 && (
                <tr className="border-b bg-gray-800 border-gray-700">
                  <td scope="row" className="px-6 py-4 font-medium text-white">
                    There are no users for this result
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <div className="w-full overflow-x-auto">
          <nav
            className="flex items-center justify-between pt-4"
            aria-label="Table navigation"
          >
            <span className="text-sm font-normal text-gray-400">
              Showing{" "}
              <span className="font-semibold text-white">
                {users.length > 0
                  ? users[0].id + " - " + users[users.length - 1].id
                  : 0 - 0}
              </span>{" "}
              of <span className="font-semibold text-white">{total}</span>
            </span>
            <div className="flex">
              <a
                onClick={prevButton}
                className="flex items-center justify-center px-3 h-8 mr-3 text-sm font-medium border rounded-lg bg-gray-800 border-gray-700 text-gray-400 hover:bg-gray-700 hover:text-white cursor-pointer space-x-1 select-none"
              >
                <ArrowLongLeftIcon className="h-6 w-5 text-gray-200" />
                <span>Previous</span>
              </a>
              <a
                onClick={nextButton}
                className="flex items-center justify-center px-3 h-8 mr-3 text-sm font-medium border rounded-lg bg-gray-800 border-gray-700 text-gray-400 hover:bg-gray-700 hover:text-white cursor-pointer space-x-1 select-none"
              >
                <span>Next</span>
                <ArrowLongRightIcon className="h-6 w-5 text-gray-200" />
              </a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
}
