import { useState } from "react";

export default function CreateUser({ setUpdate, update }) {
  const [error, setError] = useState("");
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [profession, setProfession] = useState("");
  const [devExperience, setDevExperience] = useState(0);
  const [privacyExperience, setPrivacyExperience] = useState(0);
  const [notes, setNotes] = useState("");
  let usernameTimeout;

  const checkUsername = (name) => {
    clearTimeout(usernameTimeout);
    if (!name || name.length < 1) return;
    usernameTimeout = setTimeout(async () => {
      await fetch(
        "/api/users/username?" +
          new URLSearchParams({
            username: name,
          }),
        {
          method: "get",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((res) => res.json())
        .then(({ username }) => {
          console.log(username);
          setName(name);
          setUsername(username);
        });
    }, 1000);
  };

  const createU = (e) => {
    e.preventDefault();
    if (name.length < 1) return showError("Invalid Name");
    if (email.length < 1) return showError("Invalid Email");
    fetch("/api/users", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name,
        username,
        email,
        profession,
        devExperience,
        privacyExperience,
        notes,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data?.error) return showError(data?.message);
        setUpdate(!update);
        setName("");
        setUsername("");
        setEmail("");
        setProfession("");
        setDevExperience(0);
        setPrivacyExperience(0);
        setNotes("");
      });
  };

  const showError = (message) => {
    setError(message);
    setTimeout(() => {
      setError("");
    }, 5000);
  };

  return (
    <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
      <div className="p-6 space-y-6 sm:p-8">
        <h1 className="text-xl">Create Users</h1>
        <form className="space-y-6" action="#" onSubmit={createU}>
          <div className="flex flex-wrap md:space-x-6 max-md:space-y-2">
            <div className="w-full md:w-1/3">
              <label
                htmlFor="name"
                className="block mb-2 text-sm font-medium text-white"
              >
                Full Name
              </label>
              <input
                type="text"
                name="name"
                autoComplete="off"
                className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="John Doe"
                required={true}
                onChange={(e) => checkUsername(e.target.value)}
              />
            </div>
            <div className="w-full md:w-1/3">
              <label
                htmlFor="email"
                className="block mb-2 text-sm font-medium text-white"
              >
                Username (Auto-Generated)
              </label>
              <input
                type="text"
                name="text"
                disabled={true}
                autoComplete="off"
                value={username}
                className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="john.doe"
                required={true}
              />
            </div>
          </div>
          <div className="flex flex-wrap md:space-x-6 max-md:space-y-2">
            <div className="w-full md:w-1/3">
              <label
                htmlFor="email"
                className="block mb-2 text-sm font-medium text-white"
              >
                Email
              </label>
              <input
                type="email"
                name="email"
                autoComplete="off"
                value={email}
                className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="johndoe@gmail.com"
                required={true}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="w-full md:w-1/3">
              <label
                htmlFor="email"
                className="block mb-2 text-sm font-medium text-white"
              >
                Profession
              </label>
              <input
                type="text"
                name="text"
                autoComplete="off"
                value={profession}
                className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="Lecturer"
                onChange={(e) => setProfession(e.target.value)}
              />
            </div>
          </div>
          <div className="flex flex-wrap md:space-x-6 max-md:space-y-2">
            <div className="w-full md:w-1/3">
              <label
                htmlFor="email"
                className="block mb-2 text-sm font-medium text-white"
              >
                Developer Experience (Years)
              </label>
              <input
                type="number"
                name="experience"
                autoComplete="off"
                value={devExperience}
                min={0}
                max={99}
                className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="Enter Developer Experience"
                onChange={(e) => setDevExperience(parseInt(e.target.value))}
              />
            </div>
            <div className="w-full md:w-1/3">
              <label
                htmlFor="email"
                className="block mb-2 text-sm font-medium text-white"
              >
                Privacy Experience (Years)
              </label>
              <input
                type="number"
                name="experience"
                autoComplete="off"
                value={privacyExperience}
                min={0}
                max={99}
                className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="Enter Privacy Experience"
                onChange={(e) => setPrivacyExperience(parseInt(e.target.value))}
              />
            </div>
          </div>
          <div className="flex flex-wrap md:space-x-6 max-md:space-y-2">
            <div className="w-full md:w-2/3">
              <label
                htmlFor="email"
                className="block mb-2 text-sm font-medium text-white"
              >
                Notes
              </label>
              <textarea
                type="longtext"
                name="experience"
                autoComplete="off"
                value={notes}
                className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="Enter Additional Notes"
                onChange={(e) => setNotes(e.target.value)}
              />
            </div>
          </div>
          {error.length > 0 && (
            <div
              className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
              role="alert"
            >
              <span className="block sm:inline">{error}</span>
            </div>
          )}
          <button
            type="submit"
            onClick={createU}
            className="text-white !bg-gray-700 hover:!bg-gray-600 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
          >
            Create
          </button>
        </form>
      </div>
    </div>
  );
}
