import useOutsideClick from "@/lib/useOutsideClick";
import {
  ArrowLongLeftIcon,
  ArrowLongRightIcon,
  CheckCircleIcon,
  MagnifyingGlassIcon,
  PencilIcon,
} from "@heroicons/react/24/outline";
import { useEffect, useRef, useState } from "react";

export default function PPTable({ Rupdate }) {
  const [update, setUpdate] = useState(false);
  const [pp, setPP] = useState("");
  const [description, setDescription] = useState("");
  const [pps, setPPs] = useState([]);
  const [page, setPage] = useState(1);
  const [lastpage, setLastPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [search, setSearch] = useState("");
  const [editID, setEditID] = useState(0);
  const ppref = useRef(null);
  const descref = useRef(null);
  let searchTimeout;
  useOutsideClick(ppref, ["desc", "save"], () => {
    setEditID(0);
  });
  useOutsideClick(descref, ["pp", "save"], () => {
    setEditID(0);
  });

  useEffect(() => {
    async function getRelationships() {
      await fetch(
        "/api/questions/pquestions?" +
          new URLSearchParams({
            count: 5,
            page,
            search,
          }),
        {
          method: "get",
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
        .then((res) => res.json())
        .then(({ data, lastpage, total }) => {
          console.log(data);
          setPPs(data);
          setLastPage(lastpage);
          setTotal(total);
        });
    }
    getRelationships();
  }, [page, search, update, Rupdate]);

  const doSearch = (e) => {
    clearTimeout(searchTimeout);
    searchTimeout = setTimeout(() => {
      setSearch(e.target.value);
    }, 700);
  };

  const nextButton = () => {
    if (page == lastpage) return;
    setPage(page + 1);
  };

  const prevButton = () => {
    if (page == 1) return;
    setPage(page - 1);
  };

  const toggleEditID = (id) => {
    if (editID == id) return setEditID(0);
    setPP(pps.find((p) => p.id == id).principle);
    setDescription(pps.find((p) => p.id == id).description);
    setEditID(id);
  };

  const saveEdited = (id) => {
    if (pp.length < 1) return setEditID(0);
    fetch("/api/questions/editpp", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: id,
        principle: pp,
        description,
      }),
    })
      .then((res) => res.json())
      .then(({ error, data }) => {
        !error && setUpdate(!update);
        !error && setEditID(0);
      });
  };

  return (
    <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
      <div className="p-6 space-y-6 sm:p-8">
        <h1 className="text-xl">Privacy Principles</h1>
        <span className="text-xs md:text-sm">
          These are privacy principles that generate permutations of Privacy
          Questions.
        </span>
        <div className="overflow-x-auto">
          <div className="pb-4">
            <label htmlFor="table-search" className="sr-only">
              Search
            </label>
            <div className="relative mt-1">
              <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <MagnifyingGlassIcon className="h-6 w-6 text-gray-200" />
              </div>
              <input
                type="text"
                id="table-search"
                className="m-1 p-2 pl-10 text-sm border rounded-lg w-80 bg-gray-700 border-gray-600 placeholder-gray-400"
                placeholder={"Search for principles"}
                onChange={doSearch}
              />
            </div>
          </div>
          <table className="w-full text-sm text-left text-gray-400">
            <thead className="text-xs uppercase bg-gray-700 text-gray-400">
              <tr>
                <th scope="col" className="px-6 py-3">
                  Privacy Principle ID
                </th>
                <th scope="col" className="px-6 py-3">
                  Privacy Principle
                </th>
                <th scope="col" className="px-6 py-3">
                  Privacy Principle Description
                </th>
                <th scope="col" className="px-6 py-3">
                  Edit
                </th>
              </tr>
            </thead>
            <tbody>
              {pps.length > 0 &&
                pps.map((p) => {
                  return (
                    <tr
                      className="border-b bg-gray-800 border-gray-700"
                      key={p.id}
                    >
                      <td
                        scope="row"
                        className="px-6 py-4 font-medium whitespace-nowrap text-white"
                      >
                        {p.id}
                      </td>
                      <td className="px-6 py-4">
                        {editID == p.id && (
                          <input
                            type="text"
                            name="text"
                            id="pp"
                            value={pp}
                            className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                            placeholder=""
                            required={true}
                            ref={ppref}
                            onChange={(e) => setPP(e.target.value)}
                          />
                        )}
                        {editID != p.id && p.principle}
                      </td>
                      <td className="px-6 py-4">
                        {editID == p.id && (
                          <input
                            type="text"
                            name="text"
                            id="desc"
                            value={description}
                            className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                            placeholder=""
                            required={true}
                            ref={descref}
                            onChange={(e) => setDescription(e.target.value)}
                          />
                        )}
                        {editID != p.id && (p.description.length > 1 ? p.description : "No Description")}
                      </td>
                      <td className="px-6 py-4">
                        {editID != p.id && (
                          <PencilIcon
                            className="h-6 w-6 cursor-pointer hover:text-gray-200"
                            onClick={() => toggleEditID(p.id)}
                          />
                        )}
                        {editID == p.id && (
                          <CheckCircleIcon
                            id="save"
                            className="h-6 w-6 cursor-pointer hover:text-gray-200"
                            onClick={() => saveEdited(p.id)}
                          />
                        )}
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
          <nav
            className="flex items-center justify-between pt-4"
            aria-label="Table navigation"
          >
            <span className="text-sm font-normal text-gray-400">
              Showing{" "}
              <span className="font-semibold text-white">
                {pps.length > 0
                  ? pps[0].id + " - " + pps[pps.length - 1].id
                  : 0 - 0}
              </span>{" "}
              of <span className="font-semibold text-white">{total}</span>
            </span>
            <div className="flex">
              <a
                onClick={prevButton}
                className="flex items-center justify-center px-3 h-8 mr-3 text-sm font-medium border rounded-lg bg-gray-800 border-gray-700 text-gray-400 hover:bg-gray-700 hover:text-white cursor-pointer space-x-1 select-none"
              >
                <ArrowLongLeftIcon className="h-6 w-5 text-gray-200" />
                <span>Previous</span>
              </a>
              <a
                onClick={nextButton}
                className="flex items-center justify-center px-3 h-8 mr-3 text-sm font-medium border rounded-lg bg-gray-800 border-gray-700 text-gray-400 hover:bg-gray-700 hover:text-white cursor-pointer space-x-1 select-none"
              >
                <span>Next</span>
                <ArrowLongRightIcon className="h-6 w-5 text-gray-200" />
              </a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
}
