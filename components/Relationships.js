import { useEffect, useState } from "react";

export default function Relationships({ setRelationship, pqid, rid, answered }) {
  const [relationships, setRelationships] = useState([]);
  const [selected, setSelected] = useState(rid);

  useEffect(() => {
    async function fetchRelationships() {
      await fetch("/api/questions/relationships", {
        method: "get",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then(({ data }) => {
          console.log(data);
          setRelationships(data);
        });
    }
    fetchRelationships();
  }, []);

  useEffect(() => {
    setSelected(rid);
    setRelationship({
      id: rid,
      text: rid > 0 && relationships.length > 0 ? relationships.find((r) => r.id == rid).text : "Select an option"
    });
  }, [pqid]);

  const dropdownChange = (e) => {
    setSelected(e.target.value);
    if (e.target.value > 0)
      setRelationship({
        id: parseInt(e.target.value),
        text: relationships.find((r) => r.id == e.target.value).text,
      });
    if (e.target.value <= 0)
      setRelationship({
        id: 0,
        text: "Select an option",
      });
  };

  return (
    <div className="flex flex-col items-center mb-4">
      <label htmlFor="underline_select" className="sr-only">
        Underline select
      </label>
      <select
        id="underline_select"
        className="text-center block py-2.5 px-0 w-full text-md bg-transparent border-0 border-b-2 appearance-none text-gray-400 border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer"
        value={selected}
        onChange={dropdownChange}
        disabled={answered}
      >
        <option className="bg-gray-900 text-white" value="0">
          Select an option
        </option>
        {relationships.length > 0 &&
          relationships.map((r) => (
            <option key={r.id} className="bg-gray-900 text-white" value={r.id}>
              {r.text}
            </option>
          ))}
      </select>
    </div>
  );
}
