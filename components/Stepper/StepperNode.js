import { STEPS } from "./StepperTypes";

export default function StepperNode({
  type = STEPS.MID,
  number = 0,
  completed = false,
  onClick,
  pendQ,
  pqid,
}) {
  const clickStep = () => {
    if (completed || pendQ == pqid) onClick(pqid);
  };

  return (
    <>
      {type == STEPS.START && !completed && (
        <div className="w-full">
          <span className="flex justify-center text-sm w-10 lg:w-12 whitespace-nowrap">{`ID: ${pqid}`}</span>
          <li className="flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-4 after:inline-block after:border-gray-700">
            <span
              className="flex items-center justify-center w-10 h-10 rounded-full lg:h-12 lg:w-12 bg-gray-700 shrink-0 cursor-pointer select-none"
              onClick={clickStep}
            >
              {number}
            </span>
          </li>
        </div>
      )}
      {type == STEPS.START && completed && (
        <div className="w-full">
          <span className="flex justify-center text-sm w-10 lg:w-12 whitespace-nowrap">{`${number} (ID: ${pqid})`}</span>
          <li className="flex w-full items-center text-white after:content-[''] after:w-full after:h-1 after:border-b after:border-4 after:border-green-600">
            <span
              className="flex items-center justify-center w-10 h-10 rounded-full lg:h-12 lg:w-12 bg-green-500 shrink-0 cursor-pointer select-none"
              onClick={clickStep}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5 lg:w-6 lg:h-6 text-black"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </span>
          </li>
        </div>
      )}
      {type == STEPS.END && !completed && (
        <div>
          <span className="flex justify-center text-sm w-10 lg:w-12 whitespace-nowrap">{`ID: ${pqid}`}</span>
          <li className="flex items-center">
            <span
              className="flex items-center justify-center w-10 h-10 rounded-full lg:h-12 lg:w-12 bg-gray-700 shrink-0 cursor-pointer select-none"
              onClick={clickStep}
            >
              {number}
            </span>
          </li>
        </div>
      )}
      {type == STEPS.END && completed && (
        <div>
          <span className="flex justify-center text-sm w-10 lg:w-12 whitespace-nowrap">{`${number} (ID: ${pqid})`}</span>
          <li className="flex items-center text-green-500">
            <span
              className="flex items-center justify-center w-10 h-10 rounded-full lg:h-12 lg:w-12 bg-green-500 shrink-0 cursor-pointer select-none"
              onClick={clickStep}
            >
              <svg
                aria-hidden="true"
                className="w-5 h-5 lg:w-6 lg:h-6 text-black"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </span>
          </li>
        </div>
      )}
    </>
  );
}
