import { useEffect, useState } from "react";
import StepperNode from "./StepperNode";
import { STEPS } from "./StepperTypes";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/24/solid";

// [{answered: true, number: 1}]

export default function StepperManager({
  questions,
  changeStep,
  page,
  lastPage,
  changePage,
  count,
}) {
  const [steps, setSteps] = useState([]);
  const [currentPendQ, setCurrentPendQ] = useState(0);

  useEffect(() => {
    const tempSteps = [];
    questions.forEach((q) => {
      tempSteps.push({ number: q.pqid, answered: q.answered });
    });
    setCurrentPendQ(tempSteps.find((s) => !s.answered)?.number);
    setSteps(tempSteps);
  }, [questions]);

  return (
    <div className="flex items-center">
      <div className={page != 1 ? "" : "collapse"}>
        <ChevronLeftIcon
          className="!text-white w-8 h-8 cursor-pointer select-none"
          onClick={() => changePage(page - 1)}
        />
      </div>
      <ol className="flex items-center w-full justify-center">
        {steps.length > 0 &&
          steps.map((s, i) => (
            <StepperNode
              key={s.number}
              type={steps.length != i + 1 ? STEPS.START : STEPS.END}
              completed={s.answered}
              number={count * (page - 1) + i + 1}
              pqid={s.number}
              onClick={changeStep}
              pendQ={currentPendQ}
            />
          ))}
      </ol>

      <div className={page != lastPage ? "" : "collapse"}>
        <ChevronRightIcon
          className="!text-white w-8 h-8 cursor-pointer select-none"
          onClick={() =>
            steps.length > 0 &&
            steps[steps.length - 1]?.answered &&
            changePage(page + 1)
          }
        />
      </div>
    </div>
  );
}
