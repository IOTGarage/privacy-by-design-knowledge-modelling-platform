import React, { useRef } from "react";
import Link from "next/link";
import { defaultNavItems } from "./NavItems";
import useOutsideClick from "@/lib/useOutsideClick";
import { useRouter } from "next/router";

const Sidebar = ({ open, navItems = defaultNavItems, setOpen }) => {
  const router = useRouter();
  const ref = useRef(null);
  useOutsideClick(ref, ["burger"], () => {
    setOpen(false);
  });

  return (
    <div
      className={[
        // "border-t-1 border-t-white",
        // "border-t-2 border-t-blue-300",
        "flex flex-col justify-between", // layout
        "bg-gray-800 text-zinc-50", // colors
        "md:sticky z-10 md:z-0 top-16 fixed", // positioning
        "h-[calc(100vh_-_64px)] w-[300px] md:w-full", // for height and width
        "transition-transform .3s ease-in-out md:-translate-x-0", //animations
        !open ? "-translate-x-full" : "", //hide sidebar to the left when closed
      ].join(" ")}
      ref={ref}
    >
      <nav className="md:sticky top-0 md:top-16">
        <ul className="py-2 flex flex-col gap-2">
          {navItems.map((item, index) => {
            return (
              <Link key={index} href={item.href}>
                <li
                  className={[
                    "hover:bg-blue-800", //colors
                    "flex gap-4 items-center ", //layout
                    "transition-colors duration-300", //animation
                    "rounded-md p-2 mx-2", //self style
                    router.pathname == item.href ? "bg-blue-900" : "",
                  ].join(" ")}
                >
                  {item.icon} {item.label}
                </li>
              </Link>
            );
          })}
        </ul>
      </nav>
      <Link
        href="/api/logout"
        className="top-0 md:top-16 border-t-2 border-t-white p-4 cursor-pointer hover:bg-blue-800 transition-colors duration-300"
      >
        Logout
      </Link>
    </div>
  );
};
export default Sidebar;
