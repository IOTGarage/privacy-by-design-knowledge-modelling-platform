import { Bars3Icon } from "@heroicons/react/24/outline";

export default function Sidebar({ onMenuButtonClick }) {
  return (
    <nav
      className={[
        "bg-gray-800 shadow-gray-500", // colors
        "flex items-center", // layout
        "w-full fixed z-10 px-4 shadow-sm h-16",
      ].join(" ")}
    >
      <div className="font-bold text-lg text-zinc-50">Admin Dashboard</div>
      <div className="flex-grow"></div> {/** spacer */}
      <button className="md:hidden" onClick={onMenuButtonClick}>
        <Bars3Icon className="h-6 w-6" id="burger"/>
      </button>
    </nav>
  );
}
