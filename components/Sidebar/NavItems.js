import React from "react";
import {
  ChartBarIcon,
  Cog8ToothIcon,
  LinkIcon,
  PencilIcon,
  UserGroupIcon,
} from "@heroicons/react/24/outline";

export const defaultNavItems = [
  {
    label: "Manage Users",
    href: "/admin/users",
    icon: <UserGroupIcon className="w-6 h-6" />,
  },
  {
    label: "Manage Privacy Questions",
    href: "/admin/pq",
    icon: <PencilIcon className="w-6 h-6" />,
  },
  {
    label: "Manage Relationships",
    href: "/admin/relationships",
    icon: <LinkIcon className="w-6 h-6" />,
  },
  {
    label: "Visualization",
    href: "/admin/visualize",
    icon: <ChartBarIcon className="w-6 h-6" />,
  },
  {
    label: "Settings",
    href: "/admin/settings",
    icon: <Cog8ToothIcon className="w-6 h-6" />,
  },
];
