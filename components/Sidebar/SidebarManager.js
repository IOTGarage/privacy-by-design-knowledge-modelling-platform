import { useState } from "react";
import Navbar from "./Navbar";
import Sidebar from "./Sidebar";

export default function SidebarManager({ children }) {
  const [showSidebar, setShowSidebar] = useState(false);
  return (
    <div className="grid min-h-screen grid-rows-header">
      <div className="z-10">
        <Navbar onMenuButtonClick={() => setShowSidebar(!showSidebar)} />
      </div>
      <div className="grid md:grid-cols-sidebar">
        <Sidebar open={showSidebar} setOpen={setShowSidebar} />
        <div className="p-5">{children}</div>
      </div>
    </div>
  );
}
