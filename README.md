# Privacy by Design (PbD) Knowledge Modelling Platform

This project requires [Node.js](https://nodejs.org/en) and NPM (included with Node.js).

The project uses the following frameworks:
- [NextJS](https://nextjs.org/) - A React full-stack web framework
- [Prisma](https://www.prisma.io/) - A Node.JS ORM framework
- [SQLite](https://www.sqlite.org/index.html) - Portal database engine 
- [Tailwind](https://tailwindcss.com/) - CSS framework

## How to Deploy the PbD Knowledge Modelling Platform

1. Install Node.js and NPM in the machine
2. Clone the repository
3. CD into the project
4. Run `npm install` to install dependencies
5. Run `npm run build` to setup the database with ORM and generate an optimized version of the website
6. Run `npm run start` to run the server, this will run the website on localhost:3000

Optional: Use [PM2](https://pm2.keymetrics.io/) to have the server run in the background and automatically restart on errors.

## How to Extend and Edit the Code

This project follows all the practices and recommendations from the [NextJS documentation](https://nextjs.org/docs/getting-started/project-structure).

**The `database.db` file is part of the git repository. When pulling git code to a running server, ensure the database does not get overridden. Alternatively, remove the db file from the repository**

## Understanding the Database and Deploying a fresh instance

The SQLite database uses Prisma for its ORM. The `prisma` folder contains a `schema.prisma` file that contains the schemas for all the tables. The database file `database.db` is located within the same folder.

The descriptions of every table are as follows:
- `user` - stores all user related information, including admins 
- `user_question` - stores all questions assigned to users, along with responses selected
- `pp` - stores the privacy principles
- `relationship` - stores the relationships
- `pq` - stores the privacy questions generated (combinations)
- `email_setting` - stores the settings for email sending (IMAP/GMAIL)
- `master` - stores the final responses (no foreign keys)

With any database schema changes, be sure to migrate the changes for prisma. See documentation for more details regarding [migration](https://www.prisma.io/docs/concepts/components/prisma-migrate).


## How to setup Email Configuration

There are 2 options for configuring automated email sending. SMTP and GMAIL.

SMTP requires host, email, password, port - usually for custom domain email addresses.

GMAIL requires additional steps to setup as google has restrictions for automation. Follow the steps in the article [here](https://miracleio.me/snippets/use-gmail-with-nodemailer#configuring-your-google-account) - this project uses the same library (Nodemailer) as the article.