import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method != "POST" || !req.session?.user)
    return errorResponse(req, res);
  const { id, principle, description } = await req.body;
  try {
    const check = await prisma.pP.findFirst({
      where: {
        id,
      },
    });
    if (!check) {
      return errorResponse(req, res);
    }
    const data = await prisma.pP.update({
      where: {
        id,
      },
      data: {
        principle,
        description
      },
    });
    console.log(data);
    return res.send({ error: false, data });
  } catch (message) {
    console.error(message);
    return res.status(500).send({ message });
  }
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
