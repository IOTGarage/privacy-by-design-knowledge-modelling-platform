import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method === "GET") {
    const data = await prisma.relationship.findMany();
    return res.json({ data });
  }
  if (req.method != "POST" || !req.session?.user)
    return errorResponse(req, res);
  const { relationship } = await req.body;
  try {
    const check = await prisma.relationship.findFirst({
      where: {
        text: relationship,
      },
    });
    if (check) {
      return res
        .status(401)
        .send({ error: true, message: "This relationship already exists" });
    }
    const data = await prisma.relationship.create({
      data: {
        text: relationship,
      },
    });
    console.log(data);
    return res.send({ error: false, data });
  } catch (message) {
    console.error(message);
    return res.status(500).send({ message });
  }
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
