import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method != "GET" || !req.session?.user) return errorResponse(req, res);
  const { count, page, search, userid, filterAssign, filterAns } = req.query;
  if (!count || !page) return errorResponse(req, res);
  const countnum = parseInt(count);
  const pagenum = parseInt(page);
  const total = await prisma.pQ.count({
    where: {
      ...(search && {
        OR: [
          {
            id: {
              equals: isNaN(search) ? 0 : parseInt(search),
            },
          },
          {
            pp1: {
              principle: {
                contains: search,
              },
            },
          },
          {
            pp1: {
              id: {
                equals: isNaN(search) ? 0 : parseInt(search),
              },
            },
          },
          {
            pp2: {
              principle: {
                contains: search,
              },
            },
          },
          {
            pp2: {
              id: {
                equals: isNaN(search) ? 0 : parseInt(search),
              },
            },
          },
        ],
      }),
      User_Question: {
        ...(filterAssign == "true" && {
          some: {
            userid: isNaN(userid) ? 0 : parseInt(userid),
          },
        }),
        ...(filterAns == "true" && {
          some: {
            userid: isNaN(userid) ? 0 : parseInt(userid),
            answered: true,
          },
        }),
      },
    },
  });
  const data = await prisma.pQ.findMany({
    where: {
      ...(search && {
        OR: [
          {
            id: {
              equals: isNaN(search) ? 0 : parseInt(search),
            },
          },
          {
            pp1: {
              principle: {
                contains: search,
              },
            },
          },
          {
            pp1: {
              id: {
                equals: isNaN(search) ? 0 : parseInt(search),
              },
            },
          },
          {
            pp2: {
              principle: {
                contains: search,
              },
            },
          },
          {
            pp2: {
              id: {
                equals: isNaN(search) ? 0 : parseInt(search),
              },
            },
          },
        ],
      }),
      User_Question: {
        ...(filterAssign == "true" && {
          some: {
            userid: isNaN(userid) ? 0 : parseInt(userid),
          },
        }),
        ...(filterAns == "true" && {
          some: {
            userid: isNaN(userid) ? 0 : parseInt(userid),
            answered: true,
          },
        }),
      },
    },
    skip: countnum * (pagenum - 1),
    take: countnum,
    select: {
      pp1: {
        select: {
          id: true,
          principle: true,
        },
      },
      pp2: {
        select: {
          id: true,
          principle: true,
        },
      },
      User_Question: true,
      id: true,
    },
  });
  let newd = [];
  if (data.length > 0) {
    data.forEach((d) => {
      let uq = d.User_Question.find((v) => v.userid == userid);
      newd.push({
        id: d.id,
        pp1: d.pp1,
        pp2: d.pp2,
        assigned: uq ? true : false,
        answered: uq ? uq.answered : false,
      });
    });
  }
  res.json({ data: newd, lastpage: Math.ceil(total / countnum), total });
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
