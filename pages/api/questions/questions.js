import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method != "GET" || !req.session?.user) return errorResponse(req, res);
  const { count, page } = req.query;
  if (!count || !page) return errorResponse(req, res);
  const countnum = parseInt(count);
  const pagenum = parseInt(page);
  const total = await prisma.user_Question.count({
    where: {
      userid: req.session.user.id,
    },
  });
  const data = await prisma.user_Question.findMany({
    where: {
      userid: req.session.user.id,
    },
    skip: countnum * (pagenum - 1),
    take: countnum,
    select: {
      pq: {
        select: {
          pp1: true,
          pp2: true,
        },
      },
      pqid: true,
      answered: true,
      rid: true,
      explanation: true
    },
    orderBy: {
      answered: "desc",
    },
  });
  res.json({ data, lastpage: Math.ceil(total / countnum) });
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
