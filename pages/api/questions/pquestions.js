import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method === "GET") {
    const { count, page, search } = req.query;
    if (!count || !page) return errorResponse(req, res);
    const countnum = parseInt(count);
    const pagenum = parseInt(page);
    const total = await prisma.pP.count({
      where: {
        ...(search && {
          OR: [
            {
              principle: {
                contains: search,
              },
            },
            {
              description: {
                contains: search,
              },
            },
          ],
        }),
      },
    });
    const data = await prisma.pP.findMany({
      where: {
        ...(search && {
          OR: [
            {
              principle: {
                contains: search,
              },
            },
            {
              description: {
                contains: search,
              },
            },
          ],
        }),
      },
      skip: countnum * (pagenum - 1),
      take: countnum,
    });
    return res.json({ data, lastpage: Math.ceil(total / countnum), total });
  }
  if (req.method != "POST" || !req.session?.user)
    return errorResponse(req, res);
  const { principle, description } = await req.body;
  try {
    await prisma.$transaction(async (tx) => {
      const check = await tx.pP.findFirst({
        where: {
          principle,
        },
      });
      if (check) {
        throw new Error("This principle already exists");
      }
      const data = await tx.pP.create({
        data: {
          principle,
          description,
        },
      });
      console.log(data);
      const newppid = data.id;
      const pps = await tx.pP.findMany({
        where: {
          NOT: {
            id: newppid,
          },
        },
      });
      // Generate and create combinations
      for (let i = 0; i < pps.length; i++) {
        await tx.pQ.create({
          data: {
            pp1id: newppid,
            pp2id: pps[i].id,
          },
        });
        // await tx.pQ.create({
        //   data: {
        //     pp1id: pps[i].id,
        //     pp2id: newppid,
        //   },
        // });
      }
    });
    return res.json({ success: true });
  } catch (error) {
    console.error(error);
    return res.status(500).send({ error: error.toString(), success: false });
  }
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
