import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method != "POST" || !req.session?.user)
    return errorResponse(req, res);
  const { pqid, rid, explanation } = req.body;
  if (!pqid || !rid) return errorResponse(req, res);
  const userid = req.session.user.id;

  try {
    await prisma.$transaction(async (tx) => {
      const fetchPQ = await tx.pQ.findFirst({
        where: {
          id: pqid,
        },
        select: {
          pp1: true,
          pp2: true,
        },
      });
      let fetchRelationship = { text: "I am not sure" };
      if (rid != -1) {
        fetchRelationship = await tx.relationship.findFirst({
          where: {
            id: rid,
          },
        });
      }
      const fetchUser = await tx.user.findFirst({
        where: {
          id: userid,
        },
      });
      const checkMaster = await tx.master.count({
        where: {
          name: req.session.user.name,
          pp1: fetchPQ.pp1.principle,
          pp2: fetchPQ.pp2.principle,
        },
      });
      if (checkMaster > 0) {
        throw new Error(
          "Duplicate entry found. Contact the administrator. - Name: " +
            req.session.user.name
        );
      }
      await tx.master.create({
        data: {
          email: fetchUser.email,
          name: fetchUser.name,
          profession: fetchUser.profession,
          dev_experience: fetchUser.dev_experience,
          privacy_experience: fetchUser.privacy_experience,
          notes: fetchUser.notes, 
          pp1: fetchPQ.pp1.principle,
          pp1_description: fetchPQ.pp1.description,
          pp2: fetchPQ.pp2.principle,
          pp2_description: fetchPQ.pp2.description,
          relationship: fetchRelationship.text,
          explanation
        },
      });

      // Set UQ to answered to finish
      await tx.user_Question.update({
        where: {
          userid_pqid: {
            userid,
            pqid,
          },
        },
        data: {
          answered: true,
          rid,
          explanation
        },
      });

      const checkAllSubmitted = await tx.user_Question.groupBy({
        by: ["answered"],
        where: {
          userid,
        },
        _count: {
          answered: true,
        },
      });
      // Set user "submitted" field to true if all question answered
      if (checkAllSubmitted.findIndex((a) => a.answered == false) < 0) {
        await tx.user.update({
          where: {
            id: userid,
          },
          data: {
            submitted: true,
          },
        });
      }
    });
    res.json({
      success: true,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).send({
      error: error.toString(),
      success: false,
    });
  }
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
