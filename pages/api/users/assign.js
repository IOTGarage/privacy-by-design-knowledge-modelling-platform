import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method != "POST") return errorResponse(req, res);
  const { userid, pqid, type } = req.body;
  if (type == "add") {
    let check = await prisma.user_Question.findFirst({
      where: {
        userid,
        pqid,
      },
    });
    if (check) {
      return res.json({ error: false, message: "Already Added" });
    }
    const data = await prisma.user_Question.create({
      data: {
        userid,
        pqid,
      },
    });
    return res.json({ error: false, data });
  } else if (type == "remove") {
    let check = await prisma.user_Question.findFirst({
      where: {
        userid,
        pqid,
      },
    });
    if (!check) {
      return res.json({ error: false, message: "Already Removed" });
    }
    const data = await prisma.user_Question.delete({
      where: {
        userid_pqid: {
          userid,
          pqid,
        },
      },
    });
    return res.json({ error: false, data });
  } else return errorResponse(req, res);
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
