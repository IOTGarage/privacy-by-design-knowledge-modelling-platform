import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (!req.session?.user) return errorResponse(req, res);
  if (req.method === "GET" && req.session?.user?.isAdmin) {
    const { count, page, search } = req.query;
    if (!count || !page) return errorResponse(req, res);
    const countnum = parseInt(count);
    const pagenum = parseInt(page);
    const total = await prisma.user.count({
      where: {
        admin: false,
        ...(search && {
          OR: [
            {
              username: {
                contains: search,
              },
            },
            {
              email: {
                contains: search,
              },
            },
            {
              name: {
                contains: search,
              },
            },
          ],
        }),
      },
    });
    const data = await prisma.user.findMany({
      where: {
        admin: false,
        ...(search && {
          OR: [
            {
              username: {
                contains: search,
              },
            },
            {
              email: {
                contains: search,
              },
            },
            {
              name: {
                contains: search,
              },
            },
          ],
        }),
      },
      skip: countnum * (pagenum - 1),
      take: countnum,
      select: {
        id: true,
        username: true,
        email: true,
        name: true,
        password: true,
        profession: true,
        dev_experience: true,
        privacy_experience: true,
        notes: true,
        submitted: true,
        lastreminded: true,
      },
    });
    return res.json({ data, lastpage: Math.ceil(total / countnum), total });
  }
  if (req.method != "POST" || !req.session?.user?.isAdmin)
    return errorResponse(req, res);
  const {
    name,
    username,
    email,
    profession,
    devExperience,
    privacyExperience,
    notes,
  } = req.body;
  const data = await prisma.user.create({
    data: {
      name,
      username,
      password: Math.random().toString(36).slice(-8),
      email,
      profession,
      dev_experience: devExperience,
      privacy_experience: privacyExperience,
      notes,
      lastreminded: new Date(1970, 0, 1),
    },
  });
  console.log(data);
  return res.send({ error: false });
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
