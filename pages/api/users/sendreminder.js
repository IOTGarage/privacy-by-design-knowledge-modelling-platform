import withSession from "@/lib/session";
import emailService from "@/lib/emailService";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method != "POST" || !req.session?.user)
    return errorResponse(req, res);
  const { userid } = await req.body;
  try {
    const userEmail = await prisma.user.findFirst({
      where: {
        id: userid,
      },
      select: {
        name: true,
        email: true,
        username: true,
        password: true
      }
    });
    if (!userEmail) {
      return errorResponse(req, res);
    }
    let data = await prisma.email_Setting.findFirst();
    if (!data) {
      return res.send({
        error: true,
        message: "No email settings configured",
      });
    }
    const result = await emailService.sendEmail(data, userEmail.email, "to@example.com", userEmail.name, userEmail.username, userEmail.password);
    console.log(result);
    await prisma.user.update({
        where: {
            id: userid
        },
        data: {
            lastreminded: new Date()
        }
    });
    return res.send({ error: false });
  } catch (message) {
    console.error(message);
    return res.status(500).send({ message });
  }
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
