import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method != "GET") return errorResponse(req, res);
  let { username } = req.query;
  let unique = false;
  let counter = 0;
  username = username.split(" ").join(".").toLowerCase();
  let newusername = username;
  while (!unique) {
    counter++;
    const find = await prisma.user.findUnique({
      where: {
        username: newusername,
      },
    });
    if (find) {
      // Username already exists
      newusername = username + counter;
    } else {
      unique = true;
    }
  }
  return res.json({ username: newusername });
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
