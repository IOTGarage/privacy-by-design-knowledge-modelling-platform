import withSession from "@/lib/session";

export default withSession(async (req, res) => {
  if (req.method != "GET") {
    return res
      .status(405)
      .send({
        error: "Method Not Allowed",
        isLoggedIn: req.session.hasOwnProperty("user"),
      });
  }
  req.session.destroy();
  res.redirect("/");
});
