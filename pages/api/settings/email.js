import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method === "GET") {
    const data = await prisma.email_Setting.findFirst();
    return res.json({ data });
  }
  if (req.method != "POST" || !req.session?.user)
    return errorResponse(req, res);
  const { type, email, password, host, port, secure } = await req.body;
  try {
    const check = await prisma.email_Setting.findFirst();
    let data;
    if (!check) {
      data = await prisma.email_Setting.create({
        data: {
          id: 1,
          type,
          user: email,
          password,
          host,
          port,
          secure,
        },
      });
    } else {
      data = await prisma.email_Setting.update({
        where: {
          id: 1,
        },
        data: {
          type,
          user: email,
          password,
          host,
          port,
          secure,
        },
      });
    }
    console.log(data);
    return res.send({ error: false, data });
  } catch (message) {
    console.error(message);
    return res.status(500).send({ message });
  }
});

function errorResponse(req, res) {
  return res.status(405).send({
    error: "Method Not Allowed",
    isLoggedIn: req.session.hasOwnProperty("user"),
  });
}
