import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method === "GET" && req.session?.user) {
    return res.status(200).json(req.session.user);
  }
  if (req.method != "POST")
    return res
      .status(405)
      .send({ error: "Method Not Allowed", isLoggedIn: false });
  const { username, password } = await req.body;
  try {
    const data = await prisma.user.findFirst({
      where: {
        username,
        admin: true,
      },
      select: {
        username: true,
        email: true,
        id: true,
        password: true,
      },
    });
    if (!data || data == null || password != data?.password) {
      return res
        .status(401)
        .send({ error: true, message: "Incorrect Username or Password" });
    }
    req.session.user = {
      isLoggedIn: true,
      username: data.username,
      email: data.email,
      id: data.id,
      isAdmin: true,
    };
    await req.session.save();
    return res.send({
      username: data.username,
      email: data.email,
      id: data.id,
    });
  } catch (message) {
    console.error(message);
    return res.status(500).send({ message });
  }
});
