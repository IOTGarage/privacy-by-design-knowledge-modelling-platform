import withSession from "@/lib/session";
import { prisma } from "../_base";

export default withSession(async (req, res) => {
  if (req.method === "GET" && req.session?.user) {
    return res.status(200).json(req.session.user);
  }
  if (req.method != "POST")
    return res
      .status(405)
      .send({ error: "Method Not Allowed", isLoggedIn: false });
  const { username, password } = await req.body;
  try {
    const data = await prisma.user.findFirst({
      where: {
        username,
        admin: false,
      },
      select: {
        username: true,
        name: true,
        id: true,
        email: true,
        password: true,
      },
    });
    if (!data || data == null || password != data?.password) {
      return res
        .status(401)
        .send({ error: true, message: "Incorrect Username or Password" });
    }
    req.session.user = {
      isLoggedIn: true,
      username: data.username,
      name: data.name,
      id: data.id,
      email: data.email,
      isAdmin: false,
    };
    await req.session.save();
    return res.send({
      username: data.username,
      name: data.name,
      id: data.id,
      email: data.email,
    });
  } catch (message) {
    console.time(message);
    return res.status(500).send({ message });
  }
});
