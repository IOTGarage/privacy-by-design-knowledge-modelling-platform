import useUser from "@/lib/useUser";
import { useEffect, useState } from "react";

const AdminRelationships = () => {
  const [relationships, setRelationships] = useState([]);
  const [relationship, setRelationship] = useState("");
  const [error, setError] = useState("");

  const { user } = useUser({
    adminOnly: true,
    redirectTo: "/admin/login",
    redirectIfNoAuth: true,
  });

  useEffect(() => {
    async function getRelationships() {
      await fetch("/api/questions/relationships", {
        method: "get",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((res) => res.json())
        .then(({ data }) => {
          console.log(data);
          setRelationships(data);
        });
    }
    getRelationships();
  }, []);

  const createR = async (e) => {
    e.preventDefault();
    if (relationship.length < 1) return showError("Invalid Relationship");
    await fetch("/api/questions/relationships", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ relationship }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data?.error) return showError(data.message);
        let arr = [...relationships];
        arr.push({ id: data.data.id, text: data.data.text });
        setRelationships(arr);
        setRelationship("");
      });
  };

  const showError = (message) => {
    setError(message);
    setTimeout(() => {
      setError("");
    }, 5000);
  };

  return (
    <div className="flex flex-col items-center justify-center h-full space-y-6">
      <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
        <div className="p-6 space-y-6 sm:p-8">
          <h1 className="text-xl">Relationships</h1>
          <span className="text-xs md:text-sm">
            These are between the Privacy Principles in the question. The 'I am
            not sure' checkbox is always present.
          </span>
          <div className="overflow-x-auto">
            <table className="w-full text-sm text-left text-gray-400">
              <thead className="text-xs uppercase bg-gray-700 text-gray-400">
                <tr>
                  <th scope="col" className="px-6 py-3">
                    Relationship ID
                  </th>
                  <th scope="col" className="px-6 py-3">
                    Relationship
                  </th>
                </tr>
              </thead>
              <tbody>
                {relationships.length > 0 &&
                  relationships.map((r) => {
                    return (
                      <tr
                        className="border-b bg-gray-800 border-gray-700"
                        key={r.id}
                      >
                        <td
                          scope="row"
                          className="px-6 py-4 font-medium whitespace-nowrap text-white"
                        >
                          {r.id}
                        </td>
                        <td className="px-6 py-4">{r.text}</td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
        <div className="p-6 space-y-6 sm:p-8">
          <h1 className="text-xl">Create Relationship</h1>
          <form className="space-y-6" action="#" onSubmit={createR}>
            <input
              type="text"
              name="text"
              id="text"
              value={relationship}
              className="border sm:text-sm rounded-lg block w-full md:w-1/3 p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
              placeholder="Enter Relationship"
              required={true}
              onChange={(e) => setRelationship(e.target.value)}
            />
            {error.length > 0 && (
              <div
                className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                role="alert"
              >
                <span className="block sm:inline">{error}</span>
              </div>
            )}
            <button
              type="submit"
              onClick={createR}
              className="text-white !bg-gray-700 hover:!bg-gray-600 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
            >
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

AdminRelationships.showSidebar = true;

export default AdminRelationships;
