import useUser from "@/lib/useUser";
import { useEffect, useState } from "react";
import Select from "react-select";

const AdminSettings = () => {
  const [success, setSuccess] = useState("");
  const [types, setTypes] = useState([{ name: "GMAIL" }, { name: "SMTP" }]);
  const [type, setType] = useState(types[0]);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [host, setHost] = useState("");
  const [port, setPort] = useState("");
  const [secure, setSecure] = useState(false);

  const { user } = useUser({
    adminOnly: true,
    redirectTo: "/admin/login",
    redirectIfNoAuth: true,
  });

  useEffect(() => {
    fetch("/api/settings/email", {
      method: "get",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then(({ error, data }) => {
        setType(
          types.some((t) => t.name == data.type)
            ? types.find((t) => t.name == data.type)
            : types[0]
        );
        setEmail(data.user);
        setPassword(data.password);
        setHost(data.host);
        setSecure(data.secure);
        setPort(data.port);
      });
  }, []);

  const saveB = (e) => {
    e.preventDefault();
    fetch("/api/settings/email", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        type: type.name,
        email,
        password,
        host,
        port,
        secure,
      }),
    })
      .then((res) => res.json())
      .then(({ error, data }) => {
        if (!error) setSuccess("Successfully Saved");
        setTimeout(() => {
          setSuccess("");
        }, 2000);
        console.log(data);
      });
  };

  return (
    <div className="flex flex-col items-center justify-center h-full space-y-6">
      <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
        <div className="p-6 space-y-6 sm:p-8">
          <h1 className="text-xl">Email Settings</h1>
          <form className="space-y-6" action="#" onSubmit={saveB}>
            <div className="w-full md:w-1/3">
              <label
                htmlFor="name"
                className="block mb-2 text-sm font-medium text-white"
              >
                Email Type
              </label>
              <Select
                instanceId={1}
                getOptionLabel={(e) => e.name}
                getOptionValue={(e) => e.name}
                options={types}
                isSearchable={false}
                value={type}
                classNames={{
                  control: () => "!border !border-gray-500 !bg-gray-800",
                  menu: () => "z-1",
                  menuList: () => "border border-gray-700 bg-gray-800 !p-0",
                  option: () => "!bg-gray-800 hover:!bg-gray-700 truncate",
                  singleValue: () => "!text-white",
                  noOptionsMessage: () => "!text-white",
                }}
                onChange={(e) => setType(e)}
              />
            </div>
            {type.name == "SMTP" && (
              <div className="flex flex-wrap space-x-3">
                <div>
                  <label
                    htmlFor="name"
                    className="block mb-2 text-sm font-medium text-white"
                  >
                    Host
                  </label>
                  <input
                    type="text"
                    name="text"
                    id="text"
                    value={host}
                    className="border sm:text-sm w-full rounded-lg block p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                    placeholder="smtp.example.com"
                    required={true}
                    onChange={(e) => setHost(e.target.value)}
                  />
                </div>
                <div>
                  <label
                    htmlFor="name"
                    className="block mb-2 text-sm font-medium text-white"
                  >
                    Port
                  </label>
                  <input
                    type="text"
                    name="text"
                    id="text"
                    value={port}
                    className="border sm:text-sm rounded-lg block p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white align-middle"
                    placeholder="587"
                    required={true}
                    onChange={(e) => setPort(e.target.value)}
                  />
                </div>
                <div>
                  <label
                    htmlFor="name"
                    className="block align-top mb-2 text-sm font-medium text-white"
                  >
                    Secure
                  </label>
                  <input
                    id="default-checkbox"
                    type="checkbox"
                    checked={secure}
                    onChange={() => setSecure(!secure)}
                    className="m-2.5 w-4 h-4 text-blue-600 focus:ring-blue-600 ring-offset-gray-800 focus:ring-2 bg-gray-700 border-gray-600"
                  />
                </div>
              </div>
            )}
            <div className="flex flex-wrap space-x-3">
              <div>
                <label
                  htmlFor="name"
                  className="block mb-2 text-sm font-medium text-white"
                >
                  Email
                </label>
                <input
                  type="email"
                  name="text"
                  id="text"
                  value={email}
                  className="border sm:text-sm w-full rounded-lg block p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                  placeholder="example@example.com"
                  required={true}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div>
                <label
                  htmlFor="name"
                  className="block mb-2 text-sm font-medium text-white"
                >
                  Password
                </label>
                <input
                  type="text"
                  name="text"
                  id="text"
                  value={password}
                  className="border sm:text-sm w-full rounded-lg block p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                  placeholder="........"
                  required={true}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
            </div>
            {success.length > 0 && (
              <div
                className="bg-green-100 border border-green-400 text-black px-4 py-3 rounded relative"
                role="alert"
              >
                <span className="block sm:inline">{success}</span>
              </div>
            )}
            <button
              type="submit"
              onClick={saveB}
              className="text-white !bg-gray-700 hover:!bg-gray-600 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
            >
              Save
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

AdminSettings.showSidebar = true;

export default AdminSettings;
