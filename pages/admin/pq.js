import PPTable from "@/components/Admin/PQ/PPTable";
import useUser from "@/lib/useUser";
import { useState } from "react";

const AdminHome = () => {
  const [error, setError] = useState("");
  const [pp, setPP] = useState("");
  const [description, setDescription] = useState("");
  const [update, setUpdate] = useState(false);

  const { user } = useUser({
    adminOnly: true,
    redirectTo: "/admin/login",
    redirectIfNoAuth: true,
  });

  const createPP = async (e) => {
    e.preventDefault();
    if (pp.length < 1) return showError("Invalid Principle");
    // Description can be an empty string
    await fetch("/api/questions/pquestions", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ principle: pp, description }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (!data?.success) return showError(data.error);
        data?.success && setUpdate(!update);
        data?.success && setPP("");
      });
  };

  const showError = (message) => {
    setError(message);
    setTimeout(() => {
      setError("");
    }, 5000);
  };

  return (
    <div className="flex flex-col items-center justify-center h-full space-y-6">
      <PPTable Rupdate={update} />
      <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
        <div className="p-6 space-y-6 sm:p-8">
          <h1 className="text-xl">Create Privacy Principles</h1>
          <form className="space-y-6" action="#" onSubmit={createPP}>
            <div className="flex flex-wrap md:space-x-6 max-md:space-y-2">
              <input
                type="text"
                name="text"
                id="text"
                value={pp}
                className="border sm:text-sm rounded-lg block w-full md:w-1/3 p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="Enter Privacy Principle"
                required={true}
                onChange={(e) => setPP(e.target.value)}
              />
              <input
                type="text"
                name="text"
                id="text"
                value={description}
                className="border sm:text-sm rounded-lg block w-full md:w-1/3 p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                placeholder="Enter Privacy Principle Description"
                required={true}
                onChange={(e) => setDescription(e.target.value)}
              />
            </div>
            {error.length > 0 && (
              <div
                className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                role="alert"
              >
                <span className="block sm:inline">{error}</span>
              </div>
            )}
            <button
              type="submit"
              onClick={createPP}
              className="text-white !bg-gray-700 hover:!bg-gray-600 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
            >
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

AdminHome.showSidebar = true;

export default AdminHome;
