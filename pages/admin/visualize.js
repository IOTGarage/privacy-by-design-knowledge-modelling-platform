import useUser from "@/lib/useUser";
import Link from "next/link";

const AdminHome = () => {
  const { user } = useUser({
    adminOnly: true,
    redirectTo: "/admin/login",
    redirectIfNoAuth: true,
  });

  return (
    <div className="flex flex-col items-center justify-center h-full">
      <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
        <div className="p-6 space-y-6 sm:p-8">
          <h1 className="text-xl">Visualization Page</h1>
          <span className="text-xs md:text-sm">
            Download the Jupyter Notebook and Database to start making Sankey Diagrams.
          </span>
          <div>
            <Link
              href="/jupyter/visualize.ipynb"
              locale={false}
              className="text-white !bg-gray-700 hover:!bg-gray-600 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
            >
              Download Jupyter Notebook
            </Link>
          </div>
          <div>
            <span className="text-xs md:text-sm">
              Download the database locally from the server to get the latest data.
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

AdminHome.showSidebar = true;

export default AdminHome;
