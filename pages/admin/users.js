import AssignPQ from "@/components/Admin/User/AssignPQ";
import CreateUser from "@/components/Admin/User/CreateUser";
import UsersTable from "@/components/Admin/User/UsersTable";
import useUser from "@/lib/useUser";
import { useState } from "react";

const AdminUsers = () => {
  const [update, setUpdate] = useState(false);
  const { user } = useUser({
    adminOnly: true,
    redirectTo: "/admin/login",
    redirectIfNoAuth: true,
  });

  return (
    <div className="flex flex-col items-center justify-center h-full space-y-6">
      <UsersTable update={update} setUpdate={setUpdate}/>
      <CreateUser setUpdate={setUpdate} update={update} />
      <AssignPQ />
    </div>
  );
};

AdminUsers.showSidebar = true;

export default AdminUsers;
