import CurrentQuestion from "@/components/CurrentQuestion";
import StepperManager from "@/components/Stepper/StepperManager";
import useUser from "@/lib/useUser";
import { useEffect, useState } from "react";

export default function AdminHome() {
  const [questions, setQuestions] = useState([]);
  const [error, setError] = useState("");
  const [count, setCount] = useState(3);
  const [page, setPage] = useState(1);
  const [lastPage, setLastPage] = useState(1);
  const [currentQuestion, setCurrentQuestion] = useState({
    pp1: { id: 1, principle: "This is PP1" },
    pp2: { id: 2, principle: "This is PP2" },
  });
  const [relationship, setRelationship] = useState({
    id: 0,
    text: "Select an option",
  });
  const [explanation, setExplanation] = useState("");

  const { user } = useUser({
    adminOnly: false,
    redirectTo: "/login",
    redirectIfNoAuth: true,
  });

  useEffect(() => {
    async function fetchQuestions() {
      if (page > lastPage) {
        setPage(lastPage);
        return;
      }
      const res = await fetch(
        "/api/questions/questions?" +
        new URLSearchParams({
          count,
          page,
        }),
        {
          method: "get",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const { data, lastpage } = await res.json();
      setQuestions(data);
      setCurrentQuestion(data[0]);
      setLastPage(lastpage);
      console.log(data);
    }
    user && fetchQuestions();
  }, [page, user]);

  const backButton = () => {
    const currIndex = questions.findIndex(
      (v) => v.pqid == currentQuestion.pqid
    );
    if (currIndex == 0) {
      if (page != 1) setPage(page - 1);
      return;
    }
    setCurrentQuestion(questions[currIndex - 1]);
  };

  const nextButton = async () => {
    if (relationship.id <= 0) {
      return showError("Select an option to continue");
    }
    if (!currentQuestion.answered) {
      const res = await fetch("/api/questions/response", {
        method: "post",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          pqid: currentQuestion.pqid,
          rid: relationship.id,
          explanation
        }),
      });
      const data = await res.json();
      console.log(data);
      if (data?.error)
        showError("Something went wrong. Contact the administrator.");
    }
    const currIndex = questions.findIndex(
      (v) => v.pqid == currentQuestion.pqid
    );
    if (questions.length == currIndex + 1 && page < lastPage) {
      // On last question of page which has more pages to go
      setPage(page + 1);
    } else {
      let temparr = [...questions];
      temparr[currIndex].answered = true;
      temparr[currIndex].rid = relationship.id;
      temparr[currIndex].explanation = explanation;
      setQuestions(temparr);
      if (currIndex + 1 == temparr.length) return;
      setCurrentQuestion(temparr[currIndex + 1]);
    }
  };

  const showError = (text) => {
    setError(text);
    setTimeout(() => {
      setError("");
    }, 3000);
  };

  const changeStep = (num) => {
    if (num == currentQuestion.pqid) return;
    const findInd = questions.findIndex((v) => v.pqid == num);
    if (findInd < 0) return;
    setCurrentQuestion(questions[findInd]);
  };

  return (
    <div className="min-h-screen items-center justify-between">
      <div className="flex flex-col items-center justify-center px-6 py-8 md:h-screen lg:py-0">
        <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-7xl xl:p-0 bg-gray-800 border-gray-700">
          <div className="p-6 space-y-6 sm:p-8 text-center">
            {questions && questions.length > 0 && (
              <>
                <StepperManager
                  count={count}
                  page={page}
                  lastPage={lastPage}
                  questions={questions}
                  changeStep={changeStep}
                  changePage={(p) => setPage(p)}
                />
                <h1 className="text-2xl font-bold tracking-light">
                  Question{" "}
                  {count * (page - 1) +
                    questions.findIndex((v) => v.pqid == currentQuestion.pqid) +
                    1}
                </h1>
                {error.length > 0 && (
                  <div
                    className="bg-red-100 w-full md:w-1/3 border border-red-400 text-red-700 px-1 py-2 rounded m-auto"
                    role="alert"
                  >
                    <span className="block sm:inline">{error}</span>
                  </div>
                )}
                <CurrentQuestion
                  pq={currentQuestion.pq}
                  pqid={currentQuestion.pqid}
                  setRelationship={setRelationship}
                  answered={currentQuestion.answered}
                  rid={currentQuestion.rid}
                  explanation={currentQuestion.explanation}
                  setExplanation={setExplanation}
                />
                <div className="flex space-x-4">
                  <div className="flex-auto text-end">
                    <button
                      onClick={backButton}
                      className="text-white !bg-gray-700 hover:!bg-gray-600 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                    >
                      Back
                    </button>
                  </div>
                  <div className="flex-auto text-start">
                    <button
                      onClick={nextButton}
                      className="text-white !bg-gray-700 hover:!bg-gray-600 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                    >
                      Next
                    </button>
                  </div>
                </div>
              </>
            )}
            {questions.length < 1 && (
              <h1>
                Uh-Oh! Looks like you are not assigned any questions. Please
                contact the administrator.
              </h1>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
