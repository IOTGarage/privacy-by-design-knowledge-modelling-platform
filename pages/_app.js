import { Inter } from "next/font/google";
import "../styles/globals.css";
import { SWRConfig } from "swr";
import fetch from "../lib/fetchJson";
import SidebarManager from "@/components/Sidebar/SidebarManager";

const inter = Inter({
  subsets: ["latin"],
  display: "swap",
  variable: "--font-inter",
});

export default function MyApp({ Component, pageProps }) {
  const showSidebar = Component.showSidebar ?? false;

  return (
    <SWRConfig
      value={{
        fetcher: fetch,
      }}
    >
      <main className={`${inter.variable} font-sans`}>
        {showSidebar ? (
          <SidebarManager>
            <Component {...pageProps} />
          </SidebarManager>
        ) : (
          <Component {...pageProps} />
        )}
      </main>
    </SWRConfig>
  );
}
