import useUser from "@/lib/useUser";
import Link from "next/link";
import { useState } from "react";

export default function Login({ preUsername, prePassword }) {
  const { user, mutateUser } = useUser({
    adminOnly: false,
    redirectTo: "/questions",
    redirectIfNoAuth: false,
  });

  const [username, setUsername] = useState(preUsername);
  const [password, setPassword] = useState(prePassword);
  const [error, setError] = useState("");

  const usernameForm = (e) => {
    setUsername(e.target.value);
  };

  const passwordForm = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError("");
    try {
      mutateUser(
        await fetch("/api/login/user", {
          method: "post",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ username, password }),
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            if (data?.error) setError(data.message);
            setTimeout(() => {
              setError("");
            }, 3000);
          })
      );
    } catch (error) {
      alert(error.response.data);
    }
  };

  return (
    <div className="min-h-screen items-center justify-between">
      <div className="flex flex-col items-center justify-center px-6 py-8 md:h-screen lg:py-0">
        <div className="w-full rounded-lg shadow border md:mt-0 sm:max-w-md xl:p-0 bg-gray-800 border-gray-700">
          <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
            <h1 className="text-xl font-bold text-center leading-tight tracking-tight md:text-2xl text-white">
              User Login
            </h1>
            <form
              className="space-y-4 md:space-y-6"
              action="#"
              onSubmit={handleSubmit}
            >
              <div>
                <label
                  htmlFor="username"
                  className="block mb-2 text-sm font-medium text-white"
                >
                  Username
                </label>
                <input
                  type="username"
                  name="username"
                  id="username"
                  value={username}
                  className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                  placeholder="username"
                  required={true}
                  onChange={usernameForm}
                />
              </div>
              <div>
                <label
                  htmlFor="password"
                  className="block mb-2 text-sm font-medium text-white"
                >
                  Password
                </label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  value={password}
                  placeholder="••••••••"
                  className="border sm:text-sm rounded-lg block w-full p-2.5 bg-gray-700 border-gray-600 placeholder-gray-400 text-white"
                  required={true}
                  onChange={passwordForm}
                />
              </div>
              {error.length > 0 && (
                <div
                  className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
                  role="alert"
                >
                  <span className="block sm:inline">{error}</span>
                </div>
              )}
              <button
                type="submit"
                onClick={handleSubmit}
                className="text-white !bg-gray-700 hover:!bg-gray-600 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
              >
                Sign in
              </button>
              <hr className="h-0.5 border-t-0 bg-neutral-100 opacity-50 rounded-lg" />{" "}
              <p className="text-sm text-center font-light text-gray-400">
                <Link
                  href="/admin/login"
                  className="font-medium text-white hover:underline text-primary-500"
                >
                  Admin Login
                </Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

// ?username=USERNAME&pass=PASSWORD
export const getServerSideProps = ({ query }) => {
  const preUsername = query?.username ? query.username : "";
  const prePassword = query?.pass ? query?.pass : "";
  return { props: { preUsername, prePassword } };
};
