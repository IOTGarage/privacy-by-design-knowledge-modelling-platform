import useUser from "@/lib/useUser";

export default function AdminHome() {
  const { user } = useUser({
    adminOnly: false,
    redirectTo: "/login",
    redirectIfNoAuth: true,
  });

  return <h1>Hello World</h1>;
}
